// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPNCMinTests
 * \brief Spatial parameters for the salinization problem, where evaporation
 * from a porous medium saturated wit brine and air leads to precipitation of salt.
 */

// Implementation Kosinusfunktion in Lens

#ifndef DUMUX_SALINIZATION_SPATIAL_PARAMETERS_HH
#define DUMUX_SALINIZATION_SPATIAL_PARAMETERS_HH

#include <dumux/common/math.hh>
#include <dumux/io/gnuplotinterface.hh>
#include <dumux/io/plotpckrsw.hh>
#include <dumux/io/ploteffectivediffusivitymodel.hh>
#include <dumux/io/plotthermalconductivitymodel.hh>
#include <dumux/porousmediumflow/properties.hh>
#include <dumux/porousmediumflow/fvspatialparamsmp.hh>
#include <dumux/material/fluidmatrixinteractions/2p/vangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/porosityprecipitation.hh>
#include <dumux/material/fluidmatrixinteractions/permeabilitykozenycarman.hh>
#include <random>
#include <dumux/common/random.hh>


namespace Dumux {

/*!
 * \ingroup TwoPNCMinTests
 * \brief Spatial parameters for the salinization problem, where evaporation
 * from a porous medium saturated wit brine and air leads to precipitation of salt.
 */
template<class GridGeometry, class Scalar>
class SalinizationSpatialParams
: public FVPorousMediumFlowSpatialParamsMP<GridGeometry, Scalar,
                         SalinizationSpatialParams<GridGeometry, Scalar>>
{
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using Element = typename GridView::template Codim<0>::Entity;
    using ParentType = FVPorousMediumFlowSpatialParamsMP<GridGeometry, Scalar,
                                       SalinizationSpatialParams<GridGeometry, Scalar>>;

    static constexpr int dimWorld = GridView::dimensionworld;
    using GlobalPosition = typename SubControlVolume::GlobalPosition;
    using PcKrSwCurve = FluidMatrix::VanGenuchtenDefault<Scalar>;

public:
    // type used for the permeability (i.e. tensor or scalar)
    using PermeabilityType = Scalar;
    //! Export the material law type used

    SalinizationSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    //TODO cosinus
    , K_(gridGeometry->gridView().size(0), 0.0)
    , pcKrSwCurve_("SpatialParams")
    {
        solubilityLimit_       = getParam<Scalar>("SpatialParams.SolubilityLimit", 0.26);
        referencePorosity_     = getParam<Scalar>("SpatialParams.referencePorosity", 0.11);

        //Intrinsic permeabilities
        referencePermeability_ = getParam<Scalar>("SpatialParams.referencePermeability", 2.23e-14);
        permeabilityLens_      = getParam<Scalar>("SpatialParams.PermeabilityLens", 1e-10);

        irreducibleLiqSat_     = getParam<Scalar>("SpatialParams.IrreducibleLiqSat", 0.2);
        irreducibleGasSat_     = getParam<Scalar>("SpatialParams.IrreducibleGasSat", 1e-3);
        //TODO: Cosinus function
         waveLength_            = getParam<Scalar>("Problem.InitialWaveLength");
         amplitude_             = getParam<Scalar>("Problem.Amplitude");
        

        plotFluidMatrixInteractions_ = getParam<bool>("Output.PlotFluidMatrixInteractions");


        temperature_  = getParam<Scalar>("SpatialParams.Temperature");

        lensLowerLeft_ = getParam<GlobalPosition>("SpatialParams.LensLowerLeft");
        lensUpperRight_ = getParam<GlobalPosition>("SpatialParams.LensUpperRight");
//TODO Random distribution
        std::mt19937 rand(0);
       //  Dumux::SimpleLogNormalDistribution<Scalar> K(std::log(referencePermeability_), std::log(referencePermeability_)*0.0);
       //  Dumux::SimpleLogNormalDistribution<Scalar> KLens(std::log(permeabilityLens_), std::log(permeabilityLens_)*0.1);
        //KLens = referencePermeability_ + amplitude_ * cos(2*M_PI / waveLength_ * globalPos[0]);
       // K = referencePermeability_;

     for (const auto& element : elements(gridGeometry->gridView()))
        {
            const auto eIdx = gridGeometry->elementMapper().index(element);
            const auto globalPos = element.geometry().center();
        
    if (isInLens_(element.geometry().center()))
    {
        K_[eIdx] = permeabilityLens_ + amplitude_ * cos(2*M_PI / waveLength_ * globalPos[0]);
    }
    else
    {
        K_[eIdx] = referencePermeability_;
        }
        
    }
    }
    /*!
     * \brief Returns the temperature in the domain at the given position
     * \param globalPos The position in global coordinates where the temperature should be specified.
     */
    Scalar temperatureAtPos(const GlobalPosition& globalPos) const
    {
        return temperature();
    }
    //! Return the constant temperature in the domain
    Scalar temperature() const
    { return temperature_; }

    /*!
     *  \brief Defines the minimum porosity \f$[-]\f$ distribution
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    Scalar minimalPorosity(const Element& element, const SubControlVolume &scv) const
    { return 1e-5; }

    /*!
     *  \brief Defines the volume fraction of the inert component
     *
     *  \param globalPos The global position in the domain
     *  \param compIdx The index of the inert solid component
     */
    template<class SolidSystem>
    Scalar inertVolumeFractionAtPos(const GlobalPosition& globalPos, int compIdx) const
    { return 1.0-referencePorosity_; }

    /*!
     *  \brief Defines the reference porosity \f$[-]\f$ distribution.
     *
     *  This is the porosity of the porous medium without any of the
     *  considered solid phases.
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    Scalar referencePorosity(const Element& element, const SubControlVolume &scv) const
    { return referencePorosity_; }

 //TODO random
     //We reference to the permeability field. This is used in the main function to write an output for the permeability field.
     const std::vector<Scalar>& getKField() const
        { return K_; }


    /*! Intrinsic permeability tensor K \f$[m^2]\f$ depending
     *  on the position in the domain
     *
     *  \param element The finite volume element
     *  \param scv The sub-control volume
     *  \param elemSol The element solution
     *
     *  Solution dependent permeability function
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        auto priVars = evalSolution(element, element.geometry(), elemSol, scv.center(), /*ignoreState=*/true);

        Scalar sumPrecipitates = priVars[/*numComp*/3];

        using std::max;
        const auto poro = max(/*minPoro*/1e-5, referencePorosity_ - sumPrecipitates);
        Scalar perm = K_[scv.elementIndex()];
        //TODO: random
        return permLaw_.evaluatePermeability(perm, referencePorosity_, poro);

//       if (isInLens_(element.geometry().center()))
//         if (useRandom_Permeability_){
//         int eIdx = this->gridGeometry().elementMapper().index(element);
//       priVars[permeabilityLens_] = K_[eIdx];
//        }
//            return permLaw_.evaluatePermeability(K_, referencePorosity_, poro);

//        return permLaw_.evaluatePermeability(K_, referencePorosity_, poro);

    }

    // the solubility limit of NaCl
    Scalar solubilityLimit() const
    { return solubilityLimit_; }

    Scalar theta(const SubControlVolume &scv) const
    { return 10.0; }

    /*!
     * \brief Returns the fluid-matrix interaction law at a given location
     * \param globalPos A global coordinate vector
     */
    auto fluidMatrixInteractionAtPos(const GlobalPosition &globalPos) const
    {
        return makeFluidMatrixInteraction(pcKrSwCurve_);
    }

    // define which phase is to be considered as the wetting phase
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::H2OIdx; }

    Scalar extrusionFactorAtPos(const GlobalPosition& globalPos) const
    {
        return 1.0;
    }

        /*!
     * \brief This is called from the problem and creates a gnuplot output
     *        of e.g the pc-Sw curve
     */
    void plotMaterialLaw()
    {
        GnuplotInterface<Scalar> gnuplot(plotFluidMatrixInteractions_);
        gnuplot.setOpenPlotWindow(plotFluidMatrixInteractions_);

        const auto sw = linspace(0.2, 1.0, 1000);

        const auto pc = samplePcSw(pcKrSwCurve_, sw);
        Gnuplot::addPcSw(gnuplot, sw, pc, "pc-Sw", "w lp");
        gnuplot.setOption("set xrange [0:1]");
        gnuplot.setOption("set label \"residual\\nsaturation\" at 0.1,100000 center");
        gnuplot.plot("pc-Sw");

        gnuplot.resetAll();

        const auto [krw, krn] = sampleRelPerms(makeFluidMatrixInteraction(pcKrSwCurve_), sw); // test wrapped law
        Gnuplot::addRelPerms(gnuplot, sw, krw, krn, "kr-Sw", "w lp");
        gnuplot.plot("kr");
    }


private:

    bool isInLens_(const GlobalPosition& globalPos) const
    {
        for (int i = 0; i < dimWorld; ++i){
            if (globalPos[i] < lensLowerLeft_[i] + eps_ || globalPos[i] > lensUpperRight_[i] - eps_)
                return false;
        }
        return true;
    }

    GlobalPosition lensLowerLeft_, lensUpperRight_;


     std::vector<Scalar> K_;
     //std::vector<Scalar> xBox_;

    PermeabilityKozenyCarman<PermeabilityType> permLaw_;

    Scalar solubilityLimit_;
    Scalar referencePorosity_;
    PermeabilityType referencePermeability_ = 0.0;
    PermeabilityType permeabilityLens_  = 0.0;
    Scalar irreducibleLiqSat_;
    Scalar irreducibleGasSat_;
    const PcKrSwCurve pcKrSwCurve_;
    Scalar temperature_;
    Scalar waveLength_;
    Scalar amplitude_;
    Scalar stddev_;
    static constexpr Scalar eps_ = 1e-6;


    bool plotFluidMatrixInteractions_;
   // bool useRandom_Permeability_;
};

} // end namespace Dumux

#endif

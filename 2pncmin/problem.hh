// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPNCMinTests
 * \brief Problem where water is evaporating at the top of a porous media filled
 * container saturated with brine and air, which causes precipitation at the top.
 *
 * Problem with a porous media in a container, which is open to the atmosphere
 * at the top boundary. The container has dimensions of 0.2m by 0.2m. Neumann
 * no-flow boundaries are applied at the left, right and bottom boundary.
 * The grid is refined towards the upper boundary to capter the relevant processes.
 *
 * Initially the porous medium is 50 % saturated with brine. Evaporation takes
 * place at the top boundary and hence the temperature and liquid saturation
 * decreases first at the top, then in the whole system, whereas the sodium
 * chloride (NaCl) concentration increases. This results in precipitaion of NaCl
 * at the top as the solubility limit is exceeded. Due to the low liquid saturation
 * the top after some time, top temperature rises again.
 *
 * The model uses mole fractions of dissolved components and volume fractions of
 * precipitated salt as primary variables. Make sure that the according units
 * are used in the problem set-up.
 *
 * This problem uses the \ref TwoPNCMinModel.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_stability_2pncmin</tt>
 */

#ifndef DUMUX_STABILITY_2PNC_PROBLEM_HH
#define DUMUX_STABILITY_2PNC_PROBLEM_HH

#include <random>
#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>

#include <dumux/porousmediumflow/problem.hh>

namespace Dumux {
/*!
 * \ingroup TwoPNCMinTests
 * \brief Problem where brine is evaporating at the top boundary. The system is closed at the remaining boundaries.
 */
template <class TypeTag>
class Stability2PNCProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;
    using Vertex = typename GridView::template Codim<GridView::dimensionworld>::Entity;

    enum
    {
        // primary variable indices
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

        // component indices
        xwNaClIdx = FluidSystem::NaClIdx,
        precipNaClIdx = FluidSystem::numComponents,

        // Indices of the components
        H2OIdx = FluidSystem::H2OIdx,
        NaClIdx = FluidSystem::NaClIdx,
        AirIdx = FluidSystem::AirIdx,

        // Indices of the phases
        liquidPhaseIdx = FluidSystem::liquidPhaseIdx,
        gasPhaseIdx = FluidSystem::gasPhaseIdx,

        // index of the solid phase
        sPhaseIdx = SolidSystem::comp0Idx,


        // Index of the primary component of G and L phase
        conti0EqIdx = Indices::conti0EqIdx, //water component
        conti1EqIdx = Indices::conti0EqIdx + 1, //air component
        precipNaClEqIdx = Indices::conti0EqIdx + FluidSystem::numComponents,

        #if NONISOTHERMAL
            energyEqIdx = Indices::energyEqIdx,
        #endif

        // Phase State
        bothPhases = Indices::bothPhases,
        firstPhaseOnly = Indices::firstPhaseOnly,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
    };

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using GlobalPosition = typename SubControlVolume::GlobalPosition;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;

public:
    Stability2PNCProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        //Fluidsystem
        nTemperature_           = getParam<int>("FluidSystem.NTemperature");
        nPressure_              = getParam<int>("FluidSystem.NPressure");
        pressureLow_            = getParam<Scalar>("FluidSystem.PressureLow");
        pressureHigh_           = getParam<Scalar>("FluidSystem.PressureHigh");
        temperatureLow_         = getParam<Scalar>("FluidSystem.TemperatureLow");
        temperatureHigh_        = getParam<Scalar>("FluidSystem.TemperatureHigh");


        //problem
        name_ = getParam<std::string>("Problem.Name");

        //inital conditions
        initPressure_      = getParam<Scalar>("Problem.InitialPressure");
        initSaturation_      = getParam<Scalar>("Problem.InitialSaturation");
        init_X_NaCl_          = getParam<Scalar>("Problem.InitialSaltMassFrac");
        stddev_init_x_NaCl_     = getParam<Scalar>("Problem.StddevInitialSaltMolFrac");
        useRandomInitial_x_NaCl_= getParam<bool>("Problem.UseRandomInitial_xNaCl");
        useInitialPertubations_x_NaCl_= getParam<bool>("Problem.UseInitialPertubations_xNaCl");
        evaporationRate_ = getParam<Scalar>("FreeFlow.EvaporationRate")*Dumux::Components::H2O<Scalar>::liquidMolarDensity(293.15,1e5);
        waveLength_ = getParam<Scalar>("Problem.InitialWaveLength");
        amplitude_ = getParam<Scalar>("Problem.Amplitude");

        unsigned int codim = GetPropType<TypeTag, Properties::GridGeometry>::discMethod == DiscretizationMethods::box ? dim : 0;
        permeability_.resize(gridGeometry->gridView().size(codim));

        this->spatialParams().plotMaterialLaw();

        FluidSystem::init(/*Tmin=*/temperatureLow_,
                          /*Tmax=*/temperatureHigh_,
                          /*nT=*/nTemperature_,
                          /*pmin=*/pressureLow_,
                          /*pmax=*/pressureHigh_,
                          /*np=*/nPressure_);

        if (useRandomInitial_x_NaCl_){
            unsigned seed = 1; //std::chrono::system_clock::now().time_since_epoch().count();
            std::default_random_engine generator (seed);
            std::normal_distribution<double> distribution (massToMoleFrac_(init_X_NaCl_), stddev_init_x_NaCl_);

            auto numElems = this->gridGeometry().elementMapper().size();
            x_.resize(numElems, 0.0);

            for (const auto& element : elements(this->gridGeometry().gridView()))
            {
                int eIdx = this->gridGeometry().elementMapper().index(element);
                x_[eIdx] = distribution(generator);
            }

            auto numVertex = this->gridGeometry().vertexMapper().size();
            xBox_.resize(numVertex, 0.0);

            for (const auto& vertex : vertices(this->gridGeometry().gridView()))
            {
                int vIdx = this->gridGeometry().vertexMapper().index(vertex);
                xBox_[vIdx] = distribution(generator);
            }
        }
    }

    /*!
     * \brief The current time.
     */
    void setTime( Scalar time )
    {
        time_ = time;
    }

    /*!
     * \brief The time step size.
     *
     * This is used to calculate the source term.
     */
    void setTimeStepSize( Scalar timeStepSize )
     {
        timeStepSize_ = timeStepSize;
     }

    /*!
     * \name Problem parameters
     */

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }


    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;

        // default to Neumann
        bcTypes.setAllNeumann();

        // Dirichlet bc at the column bottom
        if(globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_)
        {
            bcTypes.setAllDirichlet();
        }

        return bcTypes;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables priVars(0.0);
        priVars.setState(bothPhases);

        priVars[pressureIdx]   = initPressure_; // Bottom boundary pressure bar
        priVars[switchIdx]     = initSaturation_;
        priVars[xwNaClIdx]     = massToMoleFrac_(init_X_NaCl_);// mole fraction salt
        priVars[precipNaClIdx] = 0.0;// precipitated salt

        #if NONISOTHERMAL
            priVars[energyEqIdx] = this->spatialParams().temperature();// precipitated salt
        #endif

        return priVars;
    }


    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * This is the method for the case where the Neumann condition is
     * potentially solution dependent
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The sub control volume face
     *
     * Negative values mean influx.
     * E.g. for the mass balance that would be the mass flux in \f$ [ kg / (m^2 \cdot s)] \f$.
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {

        PrimaryVariables values(0.0);

        //evaporation starts after a certain time -> episode length
        static const Scalar episodeLength = getParam<Scalar>("TimeLoop.EpisodeLength");
        if(time_ > episodeLength){
            const auto& globalPos = scvf.ipGlobal();
            const auto& volVars = elemVolVars[scvf.insideScvIdx()];
            const Scalar hmax = this->gridGeometry().bBoxMax()[1];

            if (globalPos[1] > hmax - eps_)
            {
                // get free-flow properties:
                static const Scalar moleFracRefH2O = getParam<Scalar>("FreeFlow.RefMoleFracH2O");

                #if NONISOTHERMAL
                    static const Scalar boundaryLayerThickness = getParam<Scalar>("FreeFlow.BoundaryLayerThickness");
                    static const Scalar temperatureRef = getParam<Scalar>("FreeFlow.RefTemperature");
                #endif

                // get porous medium values:
                static const Scalar referencePermeability = getParam<Scalar>("SpatialParams.referencePermeability", 2.23e-14);

                //liquid phase
                //constant evaporation rate
                //TODO: discuss. If we are nonisothermal. Should we better use a solution dependent BC?
                values[conti0EqIdx] = evaporationRate_;

                // gas phase
                // gas flows in
                if (volVars.pressure(gasPhaseIdx) - 1e5 > 0) {
                    values[conti1EqIdx] = (volVars.pressure(gasPhaseIdx) - 1e5)
                                        /(globalPos - fvGeometry.scv(scvf.insideScvIdx()).center()).two_norm()
                                        *volVars.mobility(gasPhaseIdx)
                                        *referencePermeability
                                        *volVars.molarDensity(gasPhaseIdx)
                                        *volVars.moleFraction(gasPhaseIdx, AirIdx);
                }
                //gas flows out
                else {
                    values[conti1EqIdx] = (volVars.pressure(gasPhaseIdx) - 1e5)
                                        /(globalPos - fvGeometry.scv(scvf.insideScvIdx()).center()).two_norm()
                                        *volVars.mobility(gasPhaseIdx)
                                        *referencePermeability
                                        *volVars.molarDensity(gasPhaseIdx) * (1-moleFracRefH2O);
                }

                #if NONISOTHERMAL
                    // energy fluxes
                    values[energyEqIdx] = FluidSystem::componentEnthalpy(volVars.fluidState(), gasPhaseIdx, H2OIdx) * values[conti0EqIdx] * FluidSystem::molarMass(H2OIdx);

                    values[energyEqIdx] += FluidSystem::componentEnthalpy(volVars.fluidState(), gasPhaseIdx, AirIdx)* values[conti1EqIdx] * FluidSystem::molarMass(AirIdx);

                    values[energyEqIdx] += FluidSystem::thermalConductivity(elemVolVars[scvf.insideScvIdx()].fluidState(), gasPhaseIdx) * (volVars.temperature() - temperatureRef)/boundaryLayerThickness;
                #endif
            }
        }
        return values;
    }

    /*!
     * \brief Evaluates the initial value for a control volume.
     *
     * \param globalPos The global position
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables initial(const Element& element) const
    {
        PrimaryVariables priVars(0.0);
        priVars.setState(bothPhases);
        const auto globalPos = element.geometry().center();
        Scalar h = this->gridGeometry().bBoxMax()[1];

        //TODO: here we set the inital pertubation. This is what we are aiming to get rid off with the permeability difference in the first row. We need these imperfections to trigger the instabilites

        //TODO: method1: Just use something random. This is globally.
        //random initial salt concentration
        if (useRandomInitial_x_NaCl_){
            int eIdx = this->gridGeometry().elementMapper().index(element);
            priVars[xwNaClIdx] = x_[eIdx];
        }
        //TODO: method2: use defined imperfections
        // This only perturbs at 1/4 of top.

        else if (useInitialPertubations_x_NaCl_&& globalPos[dimWorld-1] > (h - h*0.25)){

             priVars[xwNaClIdx] = massToMoleFrac_(init_X_NaCl_ + amplitude_ * cos(2*M_PI / waveLength_ * globalPos[0]));

        }
        //TODO: method3:
        //constant initial salt concentration (globally)
        else{
            priVars[xwNaClIdx]   = massToMoleFrac_(init_X_NaCl_);     // mole fraction
        }

        priVars[pressureIdx] = 1e5;
        priVars[switchIdx] = initSaturation_;
        priVars[precipNaClIdx] = 0.0; // [kg/m^3]
        #if NONISOTHERMAL
            priVars[energyEqIdx] = this->spatialParams().temperature(); // [K]
        #endif

        return priVars;
    }

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the source term for all phases within a given
     *        sub-controlvolume.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The subcontrolvolume
     *
     * Positive values mean that the conserved quantity is created, negative ones mean that it vanishes.
     * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
     */
    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
    {
        NumEqVector source(0.0);

        const auto& volVars = elemVolVars[scv];

        Scalar moleFracNaCl_wPhase = volVars.moleFraction(liquidPhaseIdx, NaClIdx);
        Scalar massFracNaCl_Max_wPhase = this->spatialParams().solubilityLimit();
        Scalar moleFracNaCl_Max_wPhase = massToMoleFrac_(massFracNaCl_Max_wPhase); // = 0.0977248 mol NaCl/ mol Solution
        Scalar saltPorosity = this->spatialParams().minimalPorosity(element, scv);

        // precipitation of amount of salt whic hexeeds the solubility limit
        using std::abs;
        Scalar precipSalt = volVars.porosity() * volVars.molarDensity(liquidPhaseIdx)
                                               * volVars.saturation(liquidPhaseIdx)
                                               * abs(moleFracNaCl_wPhase - moleFracNaCl_Max_wPhase)
                                               / timeStepSize_;
        if (moleFracNaCl_wPhase < moleFracNaCl_Max_wPhase)
            precipSalt *= -1;

        // make sure we don't dissolve more salt than previously precipitated
        if (precipSalt*timeStepSize_ + volVars.solidVolumeFraction(sPhaseIdx)* volVars.solidComponentMolarDensity(sPhaseIdx)< 0)
            precipSalt = -volVars.solidVolumeFraction(sPhaseIdx)* volVars.solidComponentMolarDensity(sPhaseIdx)/timeStepSize_;

        // make sure there is still pore space available for precipitation
        if (volVars.solidVolumeFraction(sPhaseIdx) >= this->spatialParams().referencePorosity(element, scv) - saltPorosity  && precipSalt > 0)
            precipSalt = 0;

        source[conti0EqIdx + NaClIdx] += -precipSalt;
        source[precipNaClEqIdx] += precipSalt;
        return source;
    }

    /*!
     * \brief Adds additional VTK output data to the VTKWriter.
     *
     * Function is called by the output module on every write.
     */

    const std::vector<Scalar>& getPermeability()
    {
        return permeability_;
    }

    void updateVtkOutput(const SolutionVector& curSol)
    {
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            const auto elemSol = elementSolution(element, curSol, this->gridGeometry());

            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                VolumeVariables volVars;
                volVars.update(elemSol, *this, element, scv);
                const auto dofIdxGlobal = scv.dofIndex();
                permeability_[dofIdxGlobal] = volVars.permeability();
            }
        }
    }


private:

    /*!
     * \brief Returns the molality of NaCl (mol NaCl / kg water) for a given mole fraction.
     *
     * \param XwNaCl The XwNaCl [kg NaCl / kg solution]
     */
    static Scalar massToMoleFrac_(Scalar XwNaCl)
    {
       const Scalar Mw = 18.015e-3; //FluidSystem::molarMass(H2OIdx); /* molecular weight of water [kg/mol] */
       const Scalar Ms = 58.44e-3;  //FluidSystem::molarMass(NaClIdx); /* molecular weight of NaCl  [kg/mol] */

       const Scalar X_NaCl = XwNaCl;
       /* XwNaCl: conversion from mass fraction to mol fraction */
       auto xwNaCl = -Mw * X_NaCl / ((Ms - Mw) * X_NaCl - Ms);
       return xwNaCl;
    }


    std::string name_;

    Scalar evaporationRate_;
    Scalar initPressure_;
    Scalar initSaturation_;
    Scalar init_X_NaCl_;
    Scalar temperature_;
    Scalar stddev_init_x_NaCl_;
    bool useRandomInitial_x_NaCl_;
    bool useInitialPertubations_x_NaCl_;
    Scalar amplitude_;
    Scalar waveLength_;

    Scalar pressureLow_, pressureHigh_;
    Scalar temperatureLow_, temperatureHigh_;
    int nTemperature_;
    int nPressure_;

    Scalar time_ = 0.0;
    Scalar timeStepSize_ = 0.0;
    static constexpr Scalar eps_ = 1e-6;

    std::vector<Scalar> permeability_;

    std::vector<Scalar> x_;
    std::vector<Scalar> xBox_;
    std::vector<Scalar> y_;
    std::vector<Scalar> y2_;
};

} // end namespace Dumux

#endif

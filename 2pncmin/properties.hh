// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPNCMinTests
 * \brief The properties of the problem where brine is evaporating at the top boundary. The system is closed at the remaining boundaries.
 */
#ifndef DUMUX_STABILITY_2PNC_PROPERTIES_HH
#define DUMUX_STABILITY_2PNC_PROPERTIES_HH

//#include <dune/alugrid/grid.hh>
#include <dune/grid/yaspgrid.hh>
#include <random>

#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/method.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/box.hh>
#include <dumux/porousmediumflow/2pncmin/model.hh>
#include <dumux/custombrineair.hh>

#include <dumux/material/components/nacl.hh>
#include <dumux/material/components/granite.hh>
#include <dumux/material/solidsystems/compositionalsolidphase.hh>

#include "spatialparams.hh"
//#include "spatialparamsLens.hh" -> implemenation of lens
//#include "spatialparamsPerm.hh" -> implemenation of lens with lognormaldistr
//#include "spatialparamsPermKos.hh" -> implemenation of lens with cosinusdistr
#include "problem.hh"

namespace Dumux::Properties {

// Create new type tags
namespace TTag {
#if NONISOTHERMAL
    struct Stability2pnc { using InheritsFrom = std::tuple<TwoPNCMinNI>; };
#else
    struct Stability2pnc { using InheritsFrom = std::tuple<TwoPNCMin>; };
#endif
struct Stability2pncBox { using InheritsFrom = std::tuple<Stability2pnc,BoxModel>; };
struct Stability2pncCCTpfa { using InheritsFrom = std::tuple<Stability2pnc,CCTpfaModel>; };
} // end namespace TTag

//TODO: if you want to use dgf files, you must use ALUGRID.This becomes relevant for the periodic boundary conditions.
// Set the grid type
// template<class TypeTag>
// struct Grid<TypeTag, TTag::StabilityExperiments> { using type = Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming>; };

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::Stability2pnc>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = Dune::YaspGrid<2, Dune::TensorProductCoordinates<Scalar, 2>>;
};


// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::Stability2pnc> { using type = Stability2PNCProblem<TypeTag>; };

// Set fluid configuration
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::Stability2pnc>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::CustomBrineAir<Scalar, Components::H2O<Scalar>, FluidSystems::BrineAirDefaultPolicy</*fastButSimplifiedRelations=*/true>>;
};


template<class TypeTag>
struct SolidSystem<TypeTag, TTag::Stability2pnc>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using ComponentOne = Components::NaCl<Scalar>;
    using ComponentTwo = Components::Granite<Scalar>;
    static constexpr int numInertComponents = 1;
    using type = SolidSystems::CompositionalSolidPhase<Scalar, ComponentOne, ComponentTwo, numInertComponents>;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::Stability2pnc>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = SalinizationSpatialParams<GridGeometry, Scalar>;
};

template<class TypeTag>
struct Formulation<TypeTag, TTag::Stability2pnc>
{ static constexpr auto value = TwoPFormulation::p1s0; };

} // end namespace Dumux::Properties

#endif
